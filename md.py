from gym import spaces


md = spaces.MultiDiscrete([ [-2,-1,0,1,2 ])
for i in range(10):
  md.sample()

md.contains([4, 1, 1]) # True
md.contains([-1, 0, 0]) # False