import gym

env = gym.make('MountainCar-v0')

for episode in range(1000):
    env.render()
    action=env.action_space.sample()
    observation,reward,Isdone=env.step(action)