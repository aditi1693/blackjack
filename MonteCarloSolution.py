import numpy as np
import scipy.ndimage

import gym
env = gym.make('Blackjack-v0')
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm


def observation_divison(observation):
    return (observation[0], observation[1], int(observation[2]))


global Q

action = 0
rewards = {}
done = {}
info = {}

states = env.step(action)
print(states[0],states [1])


state_space_size = (33, 12, 2)
policy = np.zeros(state_space_size, dtype=int)


print("OSS",env.action_space.sample())

def run_episode(policy, env=env):
    steps = []
    # sum of players cards (ace counts 11)
    # sum of dealers cards
    # player has useable ace
    observation = env.reset()
    observation = observation_divison(observation)
    done = False
    steps.append(((None, None) + (observation, 0)))
    start = True
    while not done:
        if start:
            action = np.random.binomial(n=1, p=0.5)
            start = False
        else:
            action = policy[observation]
        observation_action = (observation, action)
        #action hit=1 and stick=0
        observation, reward, done, info = env.step(action)
        observation = observation_divison(observation)
        steps.append(observation_action + (observation, int(reward)))
    return steps  # list of tupels: (s, a, s', R)

size = list(state_space_size) + [2]
print(size)
Q = np.zeros(size)
print(Q.shape)

from collections import defaultdict
returns = defaultdict(list)
nb_of_episodes = 1


def monte_carlo_optimal_policy(nb_of_episodes, policy=np.random.binomial(n=1, p=0.5, size=state_space_size),run_episode=run_episode):
    for i in range(nb_of_episodes):
        # (a) generate an episode using exploring starts and pi
        observations_reward = run_episode(policy)

        G = 0  # current return
        # use a map for the first visit condition:
        o_a = {}  # map from states to (action, return)-Tuple
        for s, a, sNew, r in reversed(observations_reward):
            G = r + gamma * G
            o_a[s] = a, G

            # (b) for each pair (s,a) appearing in the episode
        for sNew, (a, G) in o_a.items():
            if sNew is not None:

                returns[(sNew, a)].append(G)
                re_mean = np.array(returns[(sNew, a)]).mean()
                Q[(sNew) + (a,)] = re_mean
                print("on")

                # for each s in the episode: optimize policy
                policy[sNew] = np.argmax(Q[(sNew)])
    return policy





gamma = 1
N = np.zeros(state_space_size, dtype=int)
rtrn = np.zeros(state_space_size)

# every visit monte carlo:
nb_of_episodes = 1000000
for e in range(nb_of_episodes):
    observations_reward = run_episode(policy)
    G = 0.
    #print (observations_reward )
    for s, a, sNew, r in reversed(observations_reward):
        G = r + gamma * G
        N[sNew] += 1
        rtrn[sNew] += G
Gs = np.zeros(state_space_size)
Gs[N!=0] = rtrn[N!=0]/N[N!=0]

def non_usable_ace_plot(Gs):
    A = np.arange(1, 11)
    B = np.arange(4, 22)
    A, B = np.meshgrid(A, B)

    V = Gs[4:22, 1:11, 0]
    fig = plt.figure(figsize=(10, 8))
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(A, B, V, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    ax.set_ylabel("Player sum")
    ax.set_xlabel("Dealer showing")
    ax.set_title("No useable Ace")
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()




def useable_ace_plot(Gs):
    A = np.arange(1, 11)
    B = np.arange(12, 22)
    A, B = np.meshgrid(A, B)

    V = Gs[12:22, 1:11, 1]
    fig = plt.figure(figsize=(10, 8))
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(A, B, V, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)
    ax.set_ylabel("Player sum")
    ax.set_xlabel("Dealer showing")
    ax.set_title("Useable Ace")
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.show()



def plot_policy_useable_ace(policy):
    B = np.arange(4 - 0.5, 22 - 0.5, 0.2)
    A = np.arange(1 - 0.5, 11 - 0.5, 0.2)
    A, B = np.meshgrid(A, B)

    Po = scipy.ndimage.zoom(policy[4:22, 1:11, 0], 5)

    levels = range(-1, 2)
    plt.figure(figsize=(7, 6))
    CS = plt.contourf(A, B, Po, levels)
    cbar = plt.colorbar(CS)
    cbar.ax.set_ylabel('actions')
    # plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Policy for no useable ace')
    plt.xlabel("dealers showing")
    plt.ylabel("Players sum")
    _ = plt.xticks(range(1, 11))
    _ = plt.yticks(range(4, 22))
    plt.show()



def plot_policy_no_useable_ace(policy):
    B = np.arange(12-0.5, 22-0.5, 0.2)
    A = np.arange(1-0.5, 11-0.5, 0.2)
    A, B = np.meshgrid(A, B)

    Po = scipy.ndimage.zoom(policy[12:22, 1:11, 1], 5, mode='nearest')

    levels = range(-1,2)
    plt.figure(figsize=(7,6))
    CS = plt.contourf(A, B, Po, levels)
    cbar = plt.colorbar(CS)
    cbar.ax.set_ylabel('actions')
    #plt.clabel(CS, inline=1, fontsize=10)
    plt.title('Policy for useable ace')
    plt.xlabel("dealers showing")
    plt.ylabel("Players sum")
    _ = plt.xticks(range(1,11))
    _ = plt.yticks(range(12,22))
    plt.show()


policy = monte_carlo_optimal_policy(nb_of_episodes)
print(policy[5, 5, 0],"policy")
non_usable_ace_plot(Gs)
useable_ace_plot(Gs)
plot_policy_useable_ace(policy)
plot_policy_no_useable_ace(policy)




