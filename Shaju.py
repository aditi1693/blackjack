import gym
from gym import spaces
from gym.utils import seeding
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


def cmp(a, b):
    return float(a > b) - float(a < b)


# 1 = Ace, 2-10 = Number cards, Jack/Queen/King = 10
deck = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]


def draw_card(np_random):
    return int(np_random.choice(deck))


def draw_hand(np_random):
    return [draw_card(np_random), draw_card(np_random)]


def usable_ace(hand):  # Does this hand have a usable ace?
    return 1 in hand and sum(hand) + 10 <= 21


def sum_hand(hand):  # Return current hand total
    if usable_ace(hand):
        return sum(hand) + 10
    return sum(hand)


def is_bust(hand):  # Is this hand a bust?
    return sum_hand(hand) > 21


def score(hand):  # What is the score of this hand (0 if bust)
    return 0 if is_bust(hand) else sum_hand(hand)


def is_natural(hand):  # Is this hand a natural blackjack?
    return sorted(hand) == [1, 10]


class BlackjackEnv(gym.Env):
    def __init__(self, natural=False):
        print("hi")
        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Tuple((
            spaces.Discrete(32),
            spaces.Discrete(11),
            spaces.Discrete(2)))
        self.seed()

        self.natural = natural
        self._get_obs()

        def seed(self, seed=None):
            self.np_random, seed = seeding.np_random(seed)
            print("hiii")
            return [seed]

        def step(self, action):

            assert self.action_space.contains(action)

            if action:  # hit: add a card to players hand and return

                self.player.append(draw_card(self.np_random))
                if is_bust(self.player):

                    done = True
                    reward = -1
                else:
                    done = False
                    reward = 0
            else:  # stick: play out the dealers hand, and score0
                done = True
                while sum_hand(self.dealer) < 17:
                    self.dealer.append(draw_card(self.np_random))
                reward = cmp(score(self.player), score(self.dealer))
                if self.natural and is_natural(self.player) and reward == 1:
                    reward = 1.5
            print("hi")
            return self._get_obs(), reward, done, {}

        def _get_obs(self):
            return (sum_hand(self.player), self.dealer[0], usable_ace(self.player))

        def reset(self):

            self.dealer = draw_hand(self.np_random)
            self.player = draw_hand(self.np_random)

            return self._get_obs()


            # env=BlackjackEnv('gym.envs.toy_text:BlackjackEnv')


env = gym.make('Blackjack-v0')

qtable = {}
count = {}
for i in range(4, 22):
    for j in range(1, 11):
        for k in range(0, 2):
            if k != 1:
                qtable[(i, j, False)] = 0
                count[(i, j, False)] = 1
            else:
                qtable[(i, j, True)] = 0
                count[(i, j, True)] = 1

for j in range(0, 10000000):
    state = env.reset()
    stack = [state]
    while (state[0] < 19):
        states = env.step(True)
        state = states[0]
        if state[0] < 22:
            stack.append(state)
    states = env.step(False)
    # print(states)
    # print(stack)
    for i in range(0, len(stack)):
        qtable[stack[i]] = qtable[stack[i]] + states[1]
        count[stack[i]] += 1



for i in range(4, 22):
    for j in range(1, 11):
        for k in range(0, 2):

            if k != 1 and count[(i, j, False)]:
                qtable[(i, j, False)] = qtable[(i, j, False)] / count[(i, j, False)]

            else:
                qtable[(i, j, True)] = qtable[(i, j, True)] / count[(i, j, True)]





fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
x_usable_ace=[]
y_usable_ace=[]
z_usable_ace=[]
x_no_usable_ace=[]
y_no_usable_ace=[]
z_no_usable_ace=[]
for i in range (12,22):
    for j in range(1,11):
          x_usable_ace.append(i)
          y_usable_ace.append(j)
          z_usable_ace.append(qtable[(i,j,True)])
          x_no_usable_ace.append(i)
          y_no_usable_ace.append(j)
          z_no_usable_ace.append(qtable[(i, j, False)])

#fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_trisurf(x_usable_ace, y_usable_ace, z_usable_ace, cmap=plt.cm.viridis, linewidth=0.2)

ax.set_xlabel("players hand")
ax.set_ylabel("dealer hand")
ax.set_zlabel("Expected reward")
plt.title("useable ace")

plt.show()

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_trisurf(x_no_usable_ace, y_no_usable_ace, z_no_usable_ace, cmap=plt.cm.viridis, linewidth=0.2)

ax.set_xlabel("players hand")
ax.set_ylabel("dealer hand")
ax.set_zlabel("Expected reward")
plt.title("no useable ace")
plt.show()


for i in range (12,22):
    for j in range(1,11):
        if qtable[(i,j,True)]>0:
             plt.plot(j,i, color='green', linestyle='dashed', linewidth = 0,
                 marker='o', markerfacecolor='green', markersize=12)
        else:
             plt.plot(j,i, color='blue', linestyle='dashed', linewidth = 0,
                 marker='o', markerfacecolor='blue', markersize=12)

plt.title("useable ace")
plt.show()


for i in range (12,22):
    for j in range(1,11):
        if qtable[(i,j,False)]>0:
             plt.plot(j,i, color='green', linestyle='dashed', linewidth = 0,
                 marker='o', markerfacecolor='green', markersize=12)
        else:
             plt.plot(j,i, color='blue', linestyle='dashed', linewidth = 0,
                 marker='o', markerfacecolor='blue', markersize=12)

plt.title("no useable ace")
plt.show()